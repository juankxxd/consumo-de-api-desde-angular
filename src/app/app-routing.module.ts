import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './vistas/dashboard/dashboard.component';
import { EditarComponent } from './vistas/editar/editar.component';
import { NuevoComponent } from './vistas/nuevo/nuevo.component';


const routes: Routes = [
  { path: '',redirectTo:'home', pathMatch:'full' },
  {path: 'home', component: DashboardComponent},
  {path: 'editar/:id', component: EditarComponent},
  {path: 'nuevo', component: NuevoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [DashboardComponent, EditarComponent, NuevoComponent]
