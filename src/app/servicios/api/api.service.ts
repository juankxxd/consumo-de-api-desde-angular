import { Injectable } from '@angular/core';
import { ResponseI } from '../../modelos/response.interface';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { objetoPokemon } from 'src/app/modelos/listapokemones.interface';
import { editPokemon } from 'src/app/modelos/editPokemon.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:string = '/api/v1/pokemones';
  prueba:string = '';
  constructor(private http: HttpClient) {
   }

   getAllPokemones():Observable<any>{
    return this.http.get<any[]>(this.url);
   }

   getSinglePokemon(id:any): Observable<editPokemon>{
     let direccion = `${this.url}/${id}`;
     this.prueba = direccion;
     return this.http.get<editPokemon>(direccion);
   }

   putPokemon(form: editPokemon): Observable<any>{
    let direccion = this.prueba;
    return this.http.put<ResponseI>(direccion, form, {observe: 'response'});
   }

   deletePokemon(form:editPokemon): Observable<ResponseI>{
    let direccion = this.prueba;
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body:form
    }
    return this.http.delete<ResponseI>(direccion, options);
   }

   postPokemon(form:editPokemon):Observable<ResponseI> {
    let direccion = this.url;
    console.log(direccion);
    return this.http.post<ResponseI>(this.url, form);
   }
}

