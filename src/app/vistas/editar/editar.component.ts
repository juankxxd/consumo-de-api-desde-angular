import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { editPokemon } from 'src/app/modelos/editPokemon.interface';
import { ResponseI } from 'src/app/modelos/response.interface';
import { ApiService } from 'src/app/servicios/api/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  otro: any;
  constructor(private activerouter: ActivatedRoute, private router: Router, private api: ApiService, private alertas: AlertasService) { }

  datospokemon: any = '';
  editarFor:any = new FormGroup({
    nombre: new FormControl(''),
    tipo: new FormControl(''),
    ataque: new FormControl(''),
    defensa: new FormControl(''),
    velocidad: new FormControl(''),
    imgs: new FormControl('')
  });

  ngOnInit(): void {
    let pokemonId = this.activerouter.snapshot.paramMap.get('id');
    this.api.getSinglePokemon(pokemonId).subscribe(data => {
      this.datospokemon = data;
      this.editarFor.setValue({
        'nombre': this.datospokemon.nombre,
        'tipo': this.datospokemon.tipo,
        'ataque': this.datospokemon.ataque,
        'defensa': this.datospokemon.defensa,
        'velocidad': this.datospokemon.velocidad,
        'imgs': this.datospokemon.imgs[0].img,
      });

      this.otro = this.editarFor.value;
    })

  }

  isObjEmpty(obj: any, objName: any) {
    for (var i in obj) {
      if (obj[i] === '') {
        return false;
      }
    }
    return true;
  }

  postForm(form: editPokemon) {
    if(this.isObjEmpty(form, 'form')) {
    this.api.putPokemon(form).subscribe(data => {
      console.log(data);
      if(data.ok){
      this.alertas.showSuccess('Datos modificados exitosamente', 'Hecho')
    } else {
      this.alertas.showError('Error', 'Error');
    }
    })
  } else {
    this.alertas.showError('No puede dejar campos vacios', 'Error');
  }
}

  eliminar() {
    let datos: editPokemon = this.editarFor.value
    this.api.deletePokemon(datos).subscribe(data => {
      let respuesta: ResponseI = data;
      this.alertas.showSuccess('Datos Eliminados exitosamente', 'Hecho')
      this.router.navigate(['home'])

    })
  }

  regresar() {
    this.router.navigate(['home']);
  }

}
