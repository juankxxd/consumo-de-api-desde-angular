import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ApiService } from 'src/app/servicios/api/api.service';
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { editPokemon } from 'src/app/modelos/editPokemon.interface';
import { ResponseI } from 'src/app/modelos/response.interface';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  otro: any;
  constructor(private activerouter: ActivatedRoute, private router: Router, private api: ApiService, private alertas: AlertasService) { }

  datospokemon: any = '';
  nuevoFor:any = new FormGroup({
    nombre: new FormControl(''),
    tipo: new FormControl(''),
    ataque: new FormControl(''),
    defensa: new FormControl(''),
    velocidad: new FormControl(''),
    imgs: new FormControl('')
  });

  ngOnInit(): void {
  }

  isObjEmpty(obj: any, objName: any) {
    for (var i in obj) {
      // obj.hasOwnProperty() se usa para filtrar propiedades de la cadena de prototipos del objeto
      if (obj[i] === '') {
        return false;
      }
    }
    return true;
  }
  postForm(form: editPokemon) {
    const formu: any = {
      nombre: form.nombre,
      tipo: form.tipo,
      ataque: form.ataque,
      defensa: form.defensa,
      velocidad: form.velocidad,
      imgs: [
        form.imgs
      ]
    };
    if (!this.isObjEmpty(form, 'form')) {
      return this.alertas.showError('No pueden haber datos vacios', 'Error');
    }
    this.api.postPokemon(formu).subscribe(data => {

      this.alertas.showSuccess('Datos modificados exitosamente', 'Hecho')

    })

  }


  regresar() {
    this.router.navigate(['home']);
  }

}

