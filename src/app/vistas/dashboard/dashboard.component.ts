import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../servicios/api/api.service';
import { Router } from '@angular/router';
import { ListaPokemonesI } from 'src/app/modelos/listapokemones.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {

  exampleString:any = '';
  pokemones:ListaPokemonesI[] = [];
  constructor(private api: ApiService, private router:Router) {
   }

  //Se ejecuta al cargar
  ngOnInit(): void {
    console.log('Hola');

    this.api.getAllPokemones().subscribe(data => {
      this.pokemones = data.pokemones;
    })
  }


  editarPokemon(id:any){
    this.router.navigate(['editar', id])
  }

  nuevoPokemon(){
    this.router.navigate(['nuevo']);
  }
}
