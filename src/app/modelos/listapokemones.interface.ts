export interface ListaPokemonesI{
  id_pokemon:string;
  nombre:string;
  tipo:string;
  ataque:number;
  defensa:number;
  velocidad:number;
}

export interface objetoPokemon{
  pokemones: ListaPokemonesI;
}
