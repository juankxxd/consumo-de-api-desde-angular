export interface editPokemon{
  nombre:string;
  tipo:string;
  ataque:number;
  defensa:number;
  velocidad:number;
  imgs:string[];
}
